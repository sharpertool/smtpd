# About

A simple smtpd server to be used during development. This can take the place of an external SMTP service, and will allow the developer to capture email output from Django, and use that as a way to get things like "validate email access" links.

